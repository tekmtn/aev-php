<?php
require_once('UserModel.base.php');
class UserModel extends UserModelBase {
	function __construct() {
		parent::__construct();
	}

	public function verifyuser($userId, $guid) {
		$where = ["userId" => $userId, "guid" => $guid, "allowPasswordReset" => 1];
		$results = $this->UserModel->get_where($where);
		if(count($results) == 1) {
			return true;
		} else {
			return false;
		}
	}
}
?>