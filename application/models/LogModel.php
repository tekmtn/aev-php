<?php
require_once('LogModel.base.php');
class LogModel extends LogModelBase {
	function __construct() {
		parent::__construct();
	}

	public function get_by_device_id($deviceId) {
		$where = ["deviceId" => $deviceId];
		$query = $this->db->get_where($this->table_name, $where);
		return $query->result_array();
	}
}
?>
