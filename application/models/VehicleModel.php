<?php
require_once('VehicleModel.base.php');
class VehicleModel extends VehicleModelBase {
	function __construct() {
		parent::__construct();
	}

	function insert_new_year($year) {
		$affectedRows = 0;

		$where = [
			"year" => $year
		];

		$query = $this->db->get_where("vehicle_makes", $where);
		if($query->num_rows() == 0) {
			$where = [
				"year" => $year - 1
			];

			$query = $this->db->get_where("vehicle_makes", $where);
			$makes = $query->result_array();
			foreach ($makes as $make) {
				$data = [
					"year" => $year,
					"make" => $make["make"],
					"code" => $make["code"],
					"createDate" => date("Y-m-d H:i:s"),
					"modifyDate" => date("Y-m-d H:i:s")
				];

				$this->db->insert("vehicle_makes", $data);
				$new_make_id = $this->db->insert_id();

				if($new_make_id > 0) {
					$affectedRows++;
				}

				$where = [
					"make_id" => $make["id"]
				];

				$query = $this->db->get_where("vehicle_models", $where);
				$models = $query->result_array();

				foreach ($models as $model) {
					$data = [
						"make_id" => $new_make_id,
						"model" => $model["model"],
						"code" => $model["code"],
						"createDate" => date("Y-m-d H:i:s"),
						"modifyDate" => date("Y-m-d H:i:s")
					];

					$this->db->insert("vehicle_models", $data);
					$new_model_id = $this->db->insert_id();

					if($new_model_id > 0) {
						$affectedRows++;
					}

					$where = [
						"model_id" => $model["id"]
					];

					$query = $this->db->get_where("vehicle_options", $where);
					$options = $query->result_array();

					foreach ($options as $option) {
						$data = [
							"model_id" => $new_model_id,
							"name" => $option["name"],
							"options" => $option["options"],
							"code" => $option["code"],
							"createDate" => date("Y-m-d H:i:s"),
							"modifyDate" => date("Y-m-d H:i:s")
						];

						$this->db->insert("vehicle_options", $data);
						$new_option_id = $this->db->insert_id();

						if($new_option_id > 0) {
							$affectedRows++;
						}
					}
				}
			}
		}

		return $affectedRows;
	}

	function insert_make($data) {
		$this->db->insert("vehicle_makes", $data);
		return $this->db->insert_id();
	}

	function insert_model($data) {
		$this->db->insert("vehicle_models", $data);
		return $this->db->insert_id();
	}

	function insert_option($data) {
		$this->db->insert("vehicle_options", $data);
		return $this->db->insert_id();
	}

	function find_make($year, $make) {
		$where = [
			"year" => $year,
			"make" => $make
		];

		$query = $this->db->get_where("vehicle_makes", $where);
		if($query->num_rows() == 1) {
			return $query->row('id');
		} else {
			return null;
		}
	}

	function find_model($make_id, $model) {
		$where = [
			"make_id" => $make_id,
			"model" => $model
		];

		$query = $this->db->get_where("vehicle_models", $where);
		if($query->num_rows() == 1) {
			return $query->row('id');
		} else {
			return null;
		}
	}

	function delete_option($option_id) {
		$this->db->delete("vehicle_options", array("id" => $option_id));
	}

	function delete_model($model_id) {
		$this->db->delete("vehicle_options", array("model_id" => $model_id));
		$this->db->delete("vehicle_models", array("id" => $model_id));
	}

	function delete_make($make_id) {
		$sql = "SELECT * FROM vehicle_models WHERE make_id = {$make_id}";
		$query = $this->db->query($sql);
		$models = $query->result_array();

		foreach ($models as $model) {
			$model_id = $model["id"];
			if(!empty($model_id)) {
				$this->db->delete("vehicle_options", array("model_id" => $model_id));
				$this->db->delete("vehicle_models", array("id" => $model_id));
			}
		}

		$this->db->delete("vehicle_makes", array("id" => $make_id));
	}

	function delete_year($year) {
		$sql = "SELECT id, make FROM vehicle_makes WHERE year = {$year}";
		$query = $this->db->query($sql);
		$makes = $query->result_array();

		foreach ($makes as $make) {
			$make_id = $make["id"];
			$sql = "SELECT * FROM vehicle_models WHERE make_id = {$make_id}";
			$query = $this->db->query($sql);
			$models = $query->result_array();

			foreach ($models as $model) {
				$model_id = $model["id"];
				$this->db->delete("vehicle_options", array("model_id" => $model_id));
				$this->db->delete("vehicle_models", array("id" => $model_id));
			}

			$this->db->delete("vehicle_makes", array("id" => $make_id, "year" => $year));
		}
	}

	public function get_tree_view() {
		$sql = "SELECT DISTINCT year FROM vehicle_makes ORDER BY year DESC";
		$query = $this->db->query($sql);
		$years = $query->result_array();

		foreach ($years as &$year) {
			$year['text'] = $year['year'];
			$sql = "SELECT id, make, code FROM vehicle_makes WHERE year = {$year['year']} ORDER BY make";
			$query = $this->db->query($sql);
			$makes = $query->result_array();

			foreach ($makes as &$make) {
				$make['text'] = $make['make'];
				$make['year'] = $year['year'];
				$make["make_id"] = $make["id"];
				$make["make_code"] = $make["code"];
				$sql = "SELECT * FROM vehicle_models WHERE make_id = {$make['id']} ORDER BY model";
				$query = $this->db->query($sql);
				$models = $query->result_array();

				foreach ($models as &$model) {
					$model['text'] = $model['model'];
					$model['make'] = $make['make'];
					$model['year'] = $year['year'];
					$model["make_id"] = $make["id"];
					$model["model_id"] = $model["id"];
					$model["model_code"] = $model["code"];
					$sql = "SELECT * FROM vehicle_options WHERE model_id = {$model['id']} ORDER BY id";
					$query = $this->db->query($sql);
					$options = $query->result_array();
					foreach ($options as &$option) {
						$option['text'] = $option['name'];
						$option['model'] = $model['model'];
						$option['make'] = $make['make'];
						$option['year'] = $year['year'];
						$option["make_id"] = $make["id"];
						$option["model_id"] = $model["id"];
						$option["option_name"] = $option["name"];
						$option["options"] = $option["options"];
						$option["option_id"] = $option['id'];
						$option["make_code"] = $make["code"];
						$option["model_code"] = $model["code"];
						$option["option_code"] = $option["code"];
					}
					$model["options"] = $options;
					$model["nodes"] = $options;
				}
				$make["models"] = $models;
				$make["nodes"] = $models;
			}
			$year["makes"] = $makes;
			$year["nodes"] = $makes;
		}

		return $years;
	}
}
?>