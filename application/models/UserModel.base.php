<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {model_name}.php
class UserModelBase extends MY_Model {
	public $table_name = 'users';
	public $primary_key_column = 'userId';
	//protected $is_deleted_column = 'isDeleted';

	function login($email, $password) {
		$sql = "SELECT * FROM " . $this->table_name . " WHERE email LIKE '" . $email . "' AND password = '" . $password . "'";
		$query = $this->db->query($sql);
		$results = $query->result_array();
		if(count($results) == 1) {
			$result = $results[0];
			$data = [ "lastLogin" => date("Y-m-d H:i:s") ];
			$this->update($data, $result[$this->primary_key_column]);
			return $result;
		} else {
			return false;
		}
	}

	function tokenLogin($email, $token) {
		$sql = "SELECT * FROM " . $this->table_name . " WHERE email LIKE '" . $email . "' AND token = '" . $token . "'";
		$query = $this->db->query($sql);
		$results = $query->result_array();
		if(count($results) == 1) {
			$result = $results[0];

			$this->session->set_userdata($result);

			$data = [ "lastLogin" => date("Y-m-d H:i:s") ];
			$this->update($data, $result[$this->primary_key_column]);
			return $result;
		} else {
			return false;
		}
	}
}
?>