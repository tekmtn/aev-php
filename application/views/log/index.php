<h3>View Logs</h3>

<table class='table table-striped'>
	<thead>
		<tr>
			<th></th>
			<th>Device Id</th>
			<th>Description</th>
			<th>Date</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($logs as $log) { ?>
		<tr>
			<td>
				<?php echo "<a href='/log/modify/" . $log["id"] . "' class='btn btn-default'>Edit</a>"; ?>
				<?php echo "<a href='/log/delete/" . $log["id"] . "' class='btn btn-default'>Delete</a>"; ?>				
			</td>
			<td><?php echo $log["deviceId"]; ?></td>
			<td><?php echo $log["description"]; ?></td>
			<td><?php echo $log["date"]; ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<!--<a href='/log/modify' class='btn btn-primary'>Create Log</a>-->