<form method='POST' class='form form-horizontal'>
	<h3>Are you sure you want to delete this log?</h3>

	<div class='veh-details'>
		<p>Description:<?php echo $log["description"]; ?></p>
		<p>Date:<?php echo $log["date"]; ?></p>
	</div>

	<button type='submit' class='btn btn-default' name='delete'>Delete Log</button>
	<a href='/logs' class='btn btn-default'>Cancel</a>
</form>