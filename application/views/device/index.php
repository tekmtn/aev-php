<h3>View Devices</h3>

<table class='table table-striped'>
	<thead>
		<tr>
			<th></th>
			<th>MAC Address</th>
			<th>Activated?</th>
			<th>VIN</th>
			<th># Logs</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($devices as $device) { ?>
		<tr>
			<td>
				<?php echo "<a href='/device/modify/" . $device["deviceId"] . "' class='btn btn-default'>Edit</a>"; ?>
				<?php echo "<a href='/device/delete/" . $device["deviceId"] . "' class='btn btn-default'>Delete</a>"; ?>				
			</td>
			<td><?php echo $device["macAddress"]; ?></td>
			<td><?php echo $device["activated"]; ?></td>
			<td><?php echo $device["associatedVIN"]; ?></td>
			<td><?php echo "<a href='/log/device/" . $device["deviceId"] . "'>" . $device["numlogs"] . "</a>"; ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<a href='/device/modify' class='btn btn-primary'>Create Device</a>