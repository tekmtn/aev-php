<?php
	if(!empty($device["deviceId"])) {
		echo "<h3>Edit Device</h3>";
	} else {
		echo "<h3>Create Device</h3>";
	}
?>

<p><a href='/devices' class='btn btn-default'>Back</a></p>

<form method="POST" class='form form-horizontal'>
	<?php
		if(!empty($device["deviceId"])) {
			echo "<input type='hidden' name='deviceId' value='" . $device["deviceId"] . "' />";
		}
	?>
	<div class='form-group'>
		<label class='control-label col-sm-2'>MAC Address</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='macAddress' value="<?php echo !empty($device) ? $device['macAddress'] : ''; ?>" />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Activated?</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='activated' value="<?php echo !empty($device) ? $device['activated'] : ''; ?>" />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>VIN</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='VIN' value="<?php echo !empty($device) ? $device['associatedVIN'] : ''; ?>" />
		</div>
	</div>
	<div class='form-group'>

		<?php if(!empty($errors)) {
			foreach ($errors as $error) {
				echo "<div class='alert alert-danger'>$error</div>";
			}
		} ?>

		<?php if(!empty($device["deviceId"])) { ?>
		<button type='submit' class='btn btn-primary'>Save Device</button>
		<?php } else { ?>
		<button type='submit' class='btn btn-primary'>Create Device</button>
		<?php } ?>
	</div>
</form>