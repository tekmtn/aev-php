<form method='POST' class='form form-horizontal'>
	<h3>Are you sure you want to delete this device?</h3>

	<div class='veh-details'>
		<p>MAC Address:<?php echo $device["macAddress"]; ?></p>
		<p>Activated:<?php echo $device["activated"]; ?></p>
		<p>VIN:<?php echo $device["associatedVIN"]; ?></p>
	</div>

	<button type='submit' class='btn btn-default' name='delete'>Delete Device</button>
	<a href='/devices' class='btn btn-default'>Cancel</a>
</form>