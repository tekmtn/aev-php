<h3>View Users</h3>

<table class='table table-striped'>
	<thead>
		<tr>
			<th></th>
			<th>Type</th>
			<th>Email</th>
			<th>First Name</th>
			<th>Last Name</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($users as $user) { ?>
		<tr>
			<td>
				<?php echo "<a href='/user/modify/" . $user["userId"] . "' class='btn btn-default'>Edit</a>"; ?>
				<?php echo "<a href='/user/delete/" . $user["userId"] . "' class='btn btn-default'>Delete</a>"; ?>				
			</td>
			<td><?php echo $user["type"]; ?></td>
			<td><?php echo $user["email"]; ?></td>
			<td><?php echo $user["firstName"]; ?></td>
			<td><?php echo $user["lastName"]; ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<a href='/user/modify' class='btn btn-primary'>Create User</a>