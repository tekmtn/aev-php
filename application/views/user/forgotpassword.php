<?php
if(!empty($message)) {
	$class = "alert-default";
	echo "<div class='alert $class'>$message</div>";
}

echo "<form method='post' class='form form-horizontal'>";
echo "<p>When you reset your password, an email will be sent to your email address with a link to change your password.</p>";
echo "<div class='form-group'>";
echo "<label class='col-sm-2 control-label'>Email:</label>";
echo "<div class='col-sm-10'><input type='email' name='email' class='form-control' /></div>";
echo "</div>";
echo "<div class='form-group'>";
echo "<div class='col-sm-offset-2 col-sm-10'>";
echo "<button type='submit' class='btn btn-primary'>Reset My Password</button>";
echo "</div>";
echo "</div>";
echo "</form>";
?>