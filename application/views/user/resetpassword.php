<?php
if(!empty($message)) {
    $class = "alert-default";
    echo "<div class='alert $class'>$message</div>";
}



if($reset === false) {
?>


<form name="resetPasswordForm" class="form-horizontal">
	<div class="form-group">
        <label class="col-sm-3 control-label">Password:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="pass" ng-model="password" required enter-clicks="reset_pwd_btn">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Repeat Password:</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="pass2" ng-model="password2" compare-to="password" enter-clicks="reset_pwd_btn">
        </div>
    </div>

    <!--<div ng-if="resetPasswordForm.pass.$error.required" class="alert alert-danger">Please include a password.</div>
    <div ng-if="resetPasswordForm.pass2.$error.compareTo" class="alert alert-danger">Passwords do not match.</div>-->

    <button type='submit' name="reset_pwd_btn" class="btn btn-primary">Reset Password</button>
</form>
<?php
} else {
    echo "<p><a href='/login'>Log Into Your Account</a></p>";
}
?>
<!--
<div ng-if="!pending && !verified">
	<p>Unable to successfully verify your account in order to reset your password.</p>
	<p>Please contact us about your account.</p>
</div>

<div ng-if="done">
	<button ng-click="login()" class="btn btn-default btn-block visible-xs">Login</button>
	<button ng-click="login()" class="btn btn-default hidden-xs">Login</button>
</div>-->