<?php
	if(!empty($user["userId"])) {
		echo "<h3>Edit User</h3>";
	} else {
		echo "<h3>Create User</h3>";
	}
?>

<p><a href='/users' class='btn btn-default'>Back</a></p>

<form method="POST" class='form form-horizontal'>
	<?php
		if(!empty($user["userId"])) {
			echo "<input type='hidden' name='userId' value='" . $user["userId"] . "' />";
		}
	?>
	<div class='form-group'>
		<label class='control-label col-sm-2'>User Type</label>
		<div class='col-sm-10'>
			<select name='type' class='form-control'>
				<option value=''></option>
				<option value='Support' <?php if(!empty($user) && $user["type"] == "Support") { echo "selected='selected'"; } ?>>Support</option>
				<option value='Admin' <?php if(!empty($user) && $user["type"] == "Admin") { echo "selected='selected'"; } ?>>Admin</option>
			</select>
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Email</label>
		<div class='col-sm-10'>
			<input type='email' class='form-control' name='email' value="<?php echo !empty($user) ? $user["email"] : ""; ?>" />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Password</label>
		<div class='col-sm-10'>
			<input type='password' class='form-control' name='password' />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>First Name</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='firstName' value="<?php echo !empty($user) ? $user["firstName"] : ""; ?>" />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Last Name</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='lastName' value="<?php echo !empty($user) ? $user["lastName"] : ""; ?>" />
		</div>
	</div>
	<div class='form-group'>

		<?php if(!empty($errors)) {
			foreach ($errors as $error) {
				echo "<div class='alert alert-danger'>$error</div>";
			}
		} ?>

		<?php if(!empty($user["userId"])) { ?>
		<button type='submit' class='btn btn-primary'>Save User</button>
		<?php } else { ?>
		<button type='submit' class='btn btn-primary'>Create User</button>
		<?php } ?>
	</div>
</form>