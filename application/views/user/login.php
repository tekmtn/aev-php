<?php
	if(!empty($message)) {
		$class = "alert-default";
		if($invalid === true) {
			$class = "alert-warning";
		}
		echo "<div class='alert $class'>$message</div>";
	}

	if(!empty($sig_request)) { 
?>
		<script type="text/javascript" src="/application/includes/duo_php/js/Duo-Web-v2.js"></script>
        <iframe id="duo_iframe" data-host="<?php echo $host; ?>" data-sig-request="<?php echo $sig_request; ?>"></iframe>
<?php
	} else {
		echo "<form method='post' class='form form-horizontal'>";
		echo "<div class='form-group'>";
		echo "<label class='col-sm-2 control-label'>Email:</label>";
		echo "<div class='col-sm-10'><input type='email' name='email' class='form-control' /></div>";
		echo "</div>";
		echo "<div class='form-group'>";
		echo "<label class='col-sm-2 control-label'>Password:</label>";
		echo "<div class='col-sm-10'><input type='password' name='pass' class='form-control' /></div>";
		echo "</div>";
		echo "<div class='form-group'>";
		echo "<div class='col-sm-offset-2 col-sm-10'>";
		echo "<p><a href='/forgotpassword' class='btn btn-link'>Forget Your Password?</a></p>";
		echo "<button type='submit' class='btn btn-primary'>Log In</button>";
		echo "</div>";
		echo "</div>";
		echo "</form>";
	}
?>