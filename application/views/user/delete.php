<form method='POST' class='form form-horizontal'>
	<h3>Are you sure you want to delete this user?</h3>

	<div class='user-details'>
		<p>User Type:<?php echo $user["type"]; ?></p>
		<p>Email:<?php echo $user["email"]; ?></p>
		<p>First Name:<?php echo $user["firstName"]; ?></p>
		<p>Last Name:<?php echo $user["lastName"]; ?></p>
	</div>

	<button type='submit' class='btn btn-default' name='delete'>Delete User</button>
	<a href='/users' class='btn btn-default'>Cancel</a>
</form>