<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
      <?php
        if(!empty($page_title)) {
          echo $page_title;
        } else {
          echo "AEV";
        }
      ?>
    </title>

    <meta name='robots' content='noindex,nofollow' />
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css" media="screen" />    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <div id="header">
        <div id="navigation">
          <?php
            $this->load->view("/layout/nav");
          ?>
        </div>
        <div class="clear"></div>
      </div><!-- /#header -->
      
      <?php
        if(isset($page_content)) {
          echo $page_content;
        }
      ?>
      
      <div id="footer">
      </div><!-- /#footer -->
    </div><!-- /#container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->

    <script src="/bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/bower_components/bootstrap-treeview/dist/bootstrap-treeview.min.js" type="text/javascript"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwbOx-tI6P3S1AV7hWZD_tviBg2KgkuHU"></script>
    <script src="/assets/js/script.js"></script>

    <?php
    if(isset($page_script)) {
      echo $page_script;
    }
    ?>
  </body>
</html>