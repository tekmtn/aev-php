<form method='POST' class='form form-horizontal'>
	<h3>Are you sure you want to delete this vehicle?</h3>

	<div class='veh-details'>
		<p>Year:<?php echo $vehicle["year"]; ?></p>
		<p>Make:<?php echo $vehicle["make"]; ?></p>
		<p>Model:<?php echo $vehicle["model"]; ?></p>
		<p>Format:<?php echo $vehicle["format"]; ?></p>
	</div>

	<button type='submit' class='btn btn-default' name='delete'>Delete Vehicle</button>
	<a href='/vehicles' class='btn btn-default'>Cancel</a>
</form>