<?php
	if(!empty($vehicle["vehicleId"])) {
		echo "<h3>Edit Vehicle</h3>";
	} else {
		echo "<h3>Create Vehicle</h3>";
	}
?>

<p><a href='/vehicles' class='btn btn-default'>Back</a></p>

<form method="POST" class='form form-horizontal'>
	<?php
		if(!empty($vehicle["vehicleId"])) {
			echo "<input type='hidden' name='vehicleId' value='" . $vehicle["vehicleId"] . "' />";
		}
	?>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Vehicle Year</label>
		<div class='col-sm-10'>
			<input type='number' class='form-control' name='year' value="<?php echo !empty($vehicle) ? $vehicle['year'] : ''; ?>" />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Make</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='make' value="<?php echo !empty($vehicle) ? $vehicle['make'] : ''; ?>" />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Model</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='model' value="<?php echo !empty($vehicle) ? $vehicle['model'] : ''; ?>" />
		</div>
	</div>
	<div class='form-group'>
		<label class='control-label col-sm-2'>Format</label>
		<div class='col-sm-10'>
			<input type='text' class='form-control' name='format' value="<?php echo !empty($vehicle) ? $vehicle['format'] : ''; ?>" />
		</div>
	</div>
	<div class='form-group'>

		<?php if(!empty($errors)) {
			foreach ($errors as $error) {
				echo "<div class='alert alert-danger'>$error</div>";
			}
		} ?>

		<?php if(!empty($vehicle["vehicleId"])) { ?>
		<button type='submit' class='btn btn-primary'>Save Vehicle</button>
		<?php } else { ?>
		<button type='submit' class='btn btn-primary'>Create Vehicle</button>
		<?php } ?>
	</div>
</form>