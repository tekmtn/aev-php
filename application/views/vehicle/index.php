<h3>View Vehicles</h3>

<div class='row'>
	<div class='col-xs-3'>
		<div id="treeview-selectable"></div>
	</div>
	<div class='col-xs-9'>

		<!--<div class='add-year-form'>
			<form method='post' class='form form-horizontal'>
				<input type='hidden' name='type' value='add_year' />
				<div class='form-group'>
					<label class='control-label col-sm-2'>Year:</label>
					<div class='col-sm-10'>
						<input type='number' name='year' class='form-control' />
					</div>
				</div>

				<button type='submit' class='btn btn-primary'>Add Year</button>
			</form>
		</div>

		<div class='add-make-form'>
			<form method='post' class='form form-horizontal'>
				<input type='hidden' name='type' value='add_make' />
				<input type='hidden' name='year' />
				<div class='form-group'>
					<label class='control-label col-sm-2'>Make:</label>
					<div class='col-sm-10'>
						<input type='text' name='make' class='form-control' />
					</div>
				</div>
			</form>
		</div>-->

		<h3 class='selected-vehicle-header'></h3>
		<h3 class='create-vehicle-header'>Create Vehicle Record</h3>

		<form method="post" class='form form-horizontal'>
			<input type='hidden' name='type' id='type' value='create' />
			<input type='hidden' id='year_id' name='year_id' />
			<input type='hidden' id='make_id' name='make_id' />
			<input type='hidden' id='model_id' name='model_id' />
			<input type='hidden' id='option_id' name='option_id' />

			<div class='form-group' id='year-input'>
				<label class='control-label col-sm-2'>Year:</label>
				<div class='col-sm-10'>
					<input type='number' class='form-control' id='year' name='year' />
				</div>
			</div>
			<div class='form-group' id='make-input'>
				<label class='control-label col-sm-2'>Make:</label>
				<div class='col-sm-10'>
					<div class='row'>
						<div class='col-xs-9'>
							<input type='text' class='form-control' id='make' name='make' />
						</div>
						<div class='col-xs-3'>
							<input type="text" class="form-control" id='make_code' name='make_code' placeholder='Make Code' />
						</div>
					</div>
				</div>
			</div>
			<div class='form-group' id='model-input'>
				<label class='control-label col-sm-2'>Model:</label>
				<div class='col-sm-10'>
					<div class='row'>
						<div class='col-xs-9'>
							<input type='text' class='form-control' id='model' name='model' />
						</div>
						<div class='col-xs-3'>
							<input type="text" class="form-control" id='model_code' name='model_code' placeholder='Model Code' />
						</div>
					</div>					
				</div>
			</div>
			<div class='form-group' id='option-input'>
				<label class='control-label col-sm-2'>Options:</label>
				<div class='col-sm-10'>
					<div class='row'>
						<div class='col-xs-9'>
							<input type='text' class='form-control' id='option_name' name='option_name' placeholder='Name of Options' />
							<textarea name='options' id='options' name='options' class='form-control' placeholder='JSON of Options'></textarea>
						</div>
						<div class='col-xs-3'>
							<input type="text" class="form-control" id='option_code' name='option_code' placeholder='Option Code' />
						</div>
					</div>						
				</div>
			</div>

			<button class='btn btn-default' id='btn-cancel' type='button'>Cancel</button>
			<button class='btn btn-danger' id='btn-delete' type='button'>Delete</button>
			<button class='btn btn-primary' id='btn-save' type='submit'>Save</button>
		</form>
	</div>
</div>





<?php
	/*echo "<div class='years'>";

	if(!empty($years)) {
		
		foreach ($years as $year) {
			echo "<div class='tree tree-years'>";
			echo "<span class='glyphicon glyphicon-minus node year-node'> " . $year["year"];

			if(!empty($year["makes"])) {
				$makes = $year["makes"];

				foreach ($makes as &$make) {
					echo "<div class='tree tree-makes'>";
					echo "<span class='glyphicon glyphicon-plus node make-node'> " . $make["make"];

					if(!empty($make["models"])) {
						$models = $make["models"];
						foreach ($models as $model) {
							echo "<div class='tree tree-models'>";
							echo "<span class='glyphicon glyphicon-plus node model-node'> " . $model["model"];

							if(!empty($model["options"])) {
								$options = $model["options"];

								foreach ($options as $option) {
									echo "<div class='tree tree-options'>";
									echo $option["options"];
									echo "</div>";
								}

								echo "<div>";
								echo "<a href='#' class='btn btn-default'>Create Options</a>";
								echo "</div>";
							}

							echo "</span>";
							echo "</div>";
						}

						echo "<div>";
						echo "<a href='#' class='btn btn-default'>Create Model</a>";
						echo "</div>";
					}

					echo "</span>";

					echo "</div>";
				}

				echo "<div>";
				echo "<a href='#' class='btn btn-default'>Create Make</a>";
				echo "</div>";
			}

			echo "</span>";

			echo "</div>";
		}
	}

	echo "<div";
	echo "<a href='#' class='btn btn-default'>Create Year</a>";
	echo "</div>";

	echo "</div>"*/
?>