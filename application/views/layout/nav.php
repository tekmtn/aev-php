<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">AEV</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href='/'>Home</a></li>
<?php
      if(empty($this->session) || !$this->session->verified) {
?>
        <li><a href='/login'>Login</a></li>
<?php
      }

      if(!empty($this->session) && $this->session->verified) {
        echo "<li><a href='/vehicles'>Vehicles</a></li>";

        if($this->session->type == "Admin") {
          echo "<li><a href='/users'>Users</a></li>";
        }
        if($this->session->type == "Support") {
          //echo "<li><a href='/tickets'>Service Tickets</a></li>";
        }

        echo "<li><a href='/log'>Logs</a></li>";
      }


      
?>
      </ul>
<?php
      if(!empty($this->session->userdata)) {
        if(!empty($this->session->email && $this->session->verified === true)) {
          echo "<ul class='nav navbar-nav navbar-right'>";
          echo "<li class='static-text'>Welcome " . $this->session->firstName . "</li>";
          echo "<li><a href='/logout'>Log Out</a></li>";
          echo "</ul>";
        }
      }
?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>