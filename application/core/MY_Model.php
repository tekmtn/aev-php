<?php
class MY_Model extends CI_Model {
	public $table_name = null;
	public $primary_key_column = null;
	//protected $is_deleted_column = 'isDeleted';

	function __construct() {
		parent::__construct();
	}

	function insert($data) {
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function get($id) {
		$query = $this->db->get_where($this->table_name, array($this->primary_key_column => $id));
		return $query->row_array();
	}

	function get_where($where) {
		$query = $this->db->get_where($this->table_name, $where);
		return $query->result_array();
	}
	
	function get_all() {
		$query = $this->db->get($this->table_name);
		return $query->result_array();
	}
	
	function get_field_data() {
		return $this->db->field_data($this->table_name);
	}

	function update($data, $id) {
		$this->db->update($this->table_name, $data, array($this->primary_key_column => $id));
	}

	function update_where($data, $where) {
		$this->db->update($this->table_name, $data, $where);
	}

	function delete($id) {
		if(!empty($this->is_deleted_column)) {
			$data = array($this->is_deleted_column => 1);
			$this->db->update($this->table_name, $data, array($this->primary_key_column => $id));
		} else {
			$this->db->delete($this->table_name, array($this->primary_key_column => $id)); 
		}
	}
}
?>