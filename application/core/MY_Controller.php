<?php
class MY_Controller extends CI_Controller {
	public $model = null;

	function __construct() {
		parent::__construct();
		//header('Access-Control-Allow-Origin: *');
		//header('Access-Control-Allow-Methods: GET, POST');

		/*if(empty($_SESSION["userId"]) && !empty($_SERVER["HTTP_X_AUTH_TOKEN"]) && !empty($_SERVER["HTTP_X_AUTH_EMAIL"])) {
			$this->functions->get_user();
		}*/
		
	}

	/*public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);

				if(!empty($post->id)) {
					$id = $post->id;

					$success = false;
					if(!empty($this->model)) {
						$this->model->delete($id);
						$success = true;
					}

					$obj = [
						"success" => $success
					];

					header('Content-Type: application/json');
					echo json_encode($obj);
				}
			}
		}
	}*/

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);

				$id = null;
				if(!empty($post->id)) {
					$id = $post->id;
				}

				if(!empty($this->model)) {
					$fields = $this->model->get_field_data();
					$non_updateable_fields = [
						$this->model->primary_key_column
					];

					$data = [];
					foreach ($fields as $field) {
						if(isset($post->{$field->name}) && !in_array($field->name, $non_updateable_fields)) {
							$data[$field->name] = $post->{$field->name};
						}
					}

					$success = false;
					if(!empty($id)) {
						$this->model->update($data, $id);
						$success = true;
					} else {
						$id = $this->model->insert($data);
						if(!empty($id)) {
							$success = true;
						}
					}

					$obj = [
						"success" => $success,
						"id" => $id
					];

					header("Content-Type: application/json");
					echo json_encode($obj);
				}
				

				/*$obj = $post->module;
				
				$data = [
				// get fields
				];

				$success = false;
				if(!empty($module->moduleId)) {
					$this->ModuleModel->update($data, $module->moduleId);
					$success = true;
				} else {
					$moduleId = $this->ModuleModel->insert($data);
					if(!empty($moduleId)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj);*/
			}
		}
	}

	public function getall() {
		$results = $this->model->get_all();
		header('Content-Type: application/json');
		echo json_encode($results, JSON_NUMERIC_CHECK);
	}
}
?>