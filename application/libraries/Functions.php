<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Functions
{
	var $obj;
	
	function __construct() {
		$this->obj =& get_instance();
	}

	public function url() {
		$cert = (!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ? true : false;
		$url = "";
		if($cert) {
			$url .= "https://";
		} else {
			$url .= "http://";
		}

		$url .= $_SERVER["HTTP_HOST"];

		return $url;
	}
	
	public function get_user() {
		$session_token = $_SERVER["HTTP_X_AUTH_TOKEN"];
		$session_email = $_SERVER["HTTP_X_AUTH_EMAIL"];

		if(!empty($session_token) && !empty($session_email)) {
			if(strpos($session_token, ", ") >= 0) {
				$parts = explode(",", $session_token);
				$session_token = trim($parts[0]);
			}

			if(strpos($session_email, ", ") >= 0) {
				$parts = explode(",", $session_email);
				$session_email = trim($parts[0]);
			}

			$this->obj->load->model("UserModel");

			$user = $this->obj->UserModel->tokenLogin($session_email, $session_token);

			if(!empty($user)) {
				$this->obj->session->set_userdata($user);
				return $user;
			} else {
				return false;
			}
		}		
	}	

	public function require_login() {
		if(!isset($this->obj->session->userdata["id"])) {
			redirect("/");
		}
	}

	public function require_admin() {
		if(empty($this->obj->session->userdata["id"]) || $this->obj->session->userdata["id"] != 1) {
			redirect("/");
		}
	}

	function sanitize($string) {
		$match = array("/\s+/","/[^a-zA-Z0-9\-]/","/-+/","/^-+/","/-+$/");
		$replace = array("-","","-","","");
		$string = preg_replace($match,$replace, $string);
		$string = strtolower($string);
		return $string;
	}
	
	public function showErrors() {
		
	}
	
	public function showMessage() {
	
	}
	
	public function sendphpmail($args) {
	//public function sendphpmail($to, $from, $fromName, $subject, $body, $attachment = '', $attachmentName = '') {
		require("class.phpmailer.php");
		//echo "HI";
		$mail = new phpmailer;
		$mail->IsSMTP();
		//$mail->SMTPDebug  = 1;
		
		/*$mail->Host = "relay-hosting.secureserver.net";
		$mail->AddAddress($to);
		$mail->From = $from;
		$mail->FromName = "TekMtn";
		$mail->Subject = $subject;
		$mail->Body = $body;*/
		
		
		
		
		$mail->SMTPSecure = "tls";
		$mail->Host = "email-smtp.us-east-1.amazonaws.com"; //"smtp.gmail.com";  // specify main and backup server
		$mail->Port = 587;
		
		$mail->SMTPAuth = true;
		$mail->Username = "AKIAIOWKMZTHXVGMH3AA"; // 'mike@tekmtn.com';
		$mail->Password = 'AoBikoAay+J+7xM9Cl7tX+EaKsNQXEWyY7uksMy8itxp';
		if(isset($args["from"]) && !empty($args["from"])) {
			$mail->From = $args["from"];
		}
		if(isset($args["bcc"]) && !empty($args["bcc"])) {
			$mail->addBCC($args["bcc"]);
		}
		//$mail->addBCC("mike.jouwstra@gmail.com", "Mike Jouwstra");
		
		if(isset($args["fromName"]) && !empty($args["fromName"])) {
			$mail->FromName = $args["fromName"];
		}
		//echo "<pre />";
		//print_r($mail);
		//die();
		if(isset($args["to"]) && !empty($args["to"])) {
			if(!is_array($args["to"])) {
				$mail->AddAddress($args["to"]);
			} else {
				foreach($args["to"] as $to) {
					$mail->AddAddress($to);
				}
			}
		}
		
		if(isset($args["subject"]) && !empty($args["subject"])) {
		
			$mail->Subject = $args["subject"];
		}
		if(isset($args["body"]) && !empty($args["body"])) {
			$mail->Body = $args["body"];
		}
		
		if(isset($args["attachment"]) && !empty($args["attachment"]) && isset($args["attachmentName"]) && !empty($args["attachmentName"])) {
			$mail->AddAttachment($args["attachment"], $args["attachmentName"]);
		}		

		$mail->IsHTML(true);    // set email format to HTML
		if(!$mail->Send())
		{
			//print_r($mail);
			//$error = 'Mail error: ' . $mail->ErrorInfo;
			//die($error);
			return false;
		} else {
			return true;
		}
	}
}
