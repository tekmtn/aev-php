<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{
    var $obj;
    var $master_layout = '';
	var $sub_layout = '';

	function __construct() {
		$this->obj =& get_instance();
	}

    function setMasterLayout($layout)
    {
    	$this->master_layout = $layout;
    }

	function setSubLayout($layout)
	{
		$this->sub_layout = $layout;
	}

    function view($view, $data=null, $return=false)
    {
		//var_dump($this);
        $loadedData = array();
		
		if(isset($data["page_title"]))
			$loadedData['page_title'] = $data["page_title"];
			
		if(isset($data["page_sidebar"]))
			$loadedData["page_sidebar"] = $data["page_sidebar"];
		if(isset($data["facebook"])) 
			$loadedData["facebook"] = $data["facebook"];
			
		if(isset($data["page_keywords"]))
			$loadedData["page_keywords"] = $data["page_keywords"];
		
		if(isset($data["page_description"]))
			$loadedData["page_description"] = $data["page_description"];
			
        $loadedData['page_content'] = $this->obj->load->view($view,$data,true); // goes into sub layout
        $loadedData['page_content'] = $this->obj->load->view($this->sub_layout, $loadedData, true);		
		if($return)
        {
			$output .= $this->obj->load->view($this->master_layout, $loadedData, true);
            return $output;
        }
        else
        {
			
            $this->obj->load->view($this->master_layout, $loadedData, false);
        }
    }
}
?>