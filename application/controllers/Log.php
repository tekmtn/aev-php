<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'Log.base.php';

class Log extends LogBase {
	function __construct() {
		parent::__construct();

		$this->layout->setMasterLayout('layout.php');
		$this->layout->setSubLayout('/log/layout.php');
	}

	public function index()
	{
		$data = [];
		$results = $this->LogModel->get_all();
		$data["logs"] = $results;
		$this->layout->view('log/index', $data);
	}

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				//$post = "test";
				$description = $post->description;

				$deviceId = 0;
				if(!empty($post->deviceId)) {
					$deviceId = $post->deviceId;
				}				
				$data = [
					"description" => $description,
					"deviceId" => $deviceId,
					"date" => date("Y-m-d H:i:s")
				];

				$okay = false;
				$id = $this->LogModel->insert($data);
				if(!empty($id)) {
					$okay = true;
				}

				//$okay = false;

				$obj = [
					"okay" => $okay
				];

				header('Content-Type: application/json');
				echo json_encode($obj);
			}
		}
	}

	public function device($deviceId) {
		$data = [];
		$data["deviceId"] = $deviceId;
		$results = $this->LogModel->get_by_device_id($deviceId);
		$data["logs"] = $results;
		$this->layout->view('log/index', $data);
	}

	/*public function delete($logId) {
		$log = $this->LogModel->get($logId);

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			if(isset($_POST["delete"])) {
				$this->LogModel->delete($logId);

				header("Location: /logs");
			}
		} else {
			if(!empty($log)) {
				$data['log'] = $log;
				$this->layout->view('log/delete', $data);
			}
		}
	}*/
}
