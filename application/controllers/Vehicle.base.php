<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {controller_name}.php
defined('BASEPATH') OR exit('No direct script access allowed');

class VehicleBase extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("VehicleModel");
		$this->model = $this->VehicleModel;
	}

	public function index()
	{

	}

	/*public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$vehicle = $post->vehicle;
				
				$success = false;
				if(!empty($vehicle->id)) {
					$this->VehicleModel->delete($vehicle->id);
					$success = true;
				}

				$obj = [
					"success" => $success,
					"vehicle" => $vehicle
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}*/

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$vehicle = $post->vehicle;
				
				$data = [
					"year" => $vehicle->year,
					"make" => $vehicle->make,
					"model" => $vehicle->model,
					"format" => $vehicle->format
				];

				$success = false;
				if(!empty($vehicle->id)) {
					$this->VehicleModel->update($data, $vehicle->id);
					$success = true;
				} else {
					$id = $this->VehicleModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function getall() {
		$results = $this->VehicleModel->get_all();
		header('Content-Type: application/json');
		echo json_encode($results, JSON_NUMERIC_CHECK);
	}


}
