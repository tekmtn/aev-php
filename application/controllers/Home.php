<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("UserModel");

		$this->layout->setMasterLayout('layout.php');
		$this->layout->setSubLayout('/home/layout.php');
	}

	public function index()
	{
		$data = [];		
		$this->layout->view('home/index', $data);
	}

	
}
