<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {controller_name}.php
defined('BASEPATH') OR exit('No direct script access allowed');

class CodeBase extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("CodeModel");
		$this->model = $this->CodeModel;
	}

	public function index()
	{

	}

	public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$code = $post->code;
				
				$success = false;
				if(!empty($code->id)) {
					$this->CodeModel->delete($code->id);
					$success = true;
				}

				$obj = [
					"success" => $success,
					"code" => $code
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$code = $post->code;
				
				$data = [
					"code" => $code->code,
					"description" => $code->description,
					"vehicleId" => $code->vehicleId
				];

				$success = false;
				if(!empty($code->id)) {
					$this->CodeModel->update($data, $code->id);
					$success = true;
				} else {
					$id = $this->CodeModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function getall() {
		$results = $this->CodeModel->get_all();
		header('Content-Type: application/json');
		echo json_encode($results, JSON_NUMERIC_CHECK);
	}


}
