<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {controller_name}.php
defined('BASEPATH') OR exit('No direct script access allowed');

class SettingBase extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("SettingModel");
	}

	public function index()
	{

	}

	public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$setting = $post->setting;
				
				$success = false;
				if(!empty($setting->id)) {
					$this->SettingModel->delete($setting->id);
					$success = true;
				}

				$obj = [
					"success" => $success,
					"setting" => $setting
				];

				header('Content-Type: application/json');
				echo json_encode($obj);
			}
		}
	}

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$setting = $post->setting;
				
				$data = [

				];

				$success = false;
				if(!empty($setting->id)) {
					$this->SettingModel->update($data, $setting->id);
					$success = true;
				} else {
					$id = $this->SettingModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj);
			}
		}
	}

	public function getall() {
		$results = $this->SettingModel->get_all();
		header('Content-Type: application/json');
		echo json_encode($results);
	}


}
