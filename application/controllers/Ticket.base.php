<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {controller_name}.php
defined('BASEPATH') OR exit('No direct script access allowed');

class TicketBase extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("TicketModel");
		$this->model = $this->TicketModel;
	}

	public function index()
	{

	}

	public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$ticket = $post->ticket;
				
				$success = false;
				if(!empty($ticket->id)) {
					$this->TicketModel->delete($ticket->id);
					$success = true;
				}

				$obj = [
					"success" => $success,
					"ticket" => $ticket
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$ticket = $post->ticket;
				
				$data = [
					"assignedToUserId" => $ticket->assignedToUserId,
					"resolved" => $ticket->resolved
				];

				$success = false;
				if(!empty($ticket->id)) {
					$this->TicketModel->update($data, $ticket->id);
					$success = true;
				} else {
					$id = $this->TicketModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function getall() {
		$results = $this->TicketModel->get_all();
		header('Content-Type: application/json');
		echo json_encode($results, JSON_NUMERIC_CHECK);
	}


}
