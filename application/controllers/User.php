<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'User.base.php';

class User extends UserBase {
	function __construct() {
		parent::__construct();

		$this->layout->setMasterLayout('layout.php');
		$this->layout->setSubLayout('/user/layout.php');
	}

	public function index()
	{
		if(empty($this->session->userdata) || $this->session->type != "Admin") {
			return;
		}

		$data = [];
		$results = $this->UserModel->get_all();
		$data["users"] = $results;
		$this->layout->view('user/index', $data);
	}

	public function delete($userId) {
		$user = $this->UserModel->get($userId);

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			if(isset($_POST["delete"])) {
				$this->UserModel->delete($userId);

				header("Location: /users");
			}
		} else {
			if(!empty($user)) {
				$data['user'] = $user;
				$this->layout->view('user/delete', $data);
			}
		}
	}

	public function modify($userId = null) {
		if(empty($this->session->userdata) || $this->session->type != "Admin") {
			return;
		}

		if($_SERVER["REQUEST_METHOD"] == "POST") {

			$errors = [];
			if(empty($_POST["type"])) {
				$errors[] = "Please select a user type.";
			}
			if(empty($_POST["email"])) {
				$errors[] = "Please enter the user's email.";
			}
			if(empty($_POST["firstName"])) {
				$errors[] = "Please enter the user's first name.";
			}
			if(empty($_POST["lastName"])) {
				$errors[] = "Please enter the user's last name.";
			}
			if(empty($_POST["password"])) {
				$errors[] = "Please include a password for the user.";
			}

			$data["user"] = [];

			$data["user"]["type"] = $_POST["type"];
			$data["user"]["email"] = $_POST["email"];
			$data["user"]["firstName"] = $_POST["firstName"];
			$data["user"]["lastName"] = $_POST["lastName"];
			$data["user"]["password"] = $_POST["password"];

			if(count($errors) == 0) {
				$id = $_POST["userId"];
				$type = $_POST["type"];
				$email = $_POST["email"];
				$firstName = $_POST["firstName"];
				$lastName = $_POST["lastName"];
				$password = $_POST["password"];

				$obj = [
					"type" => $type,
					"email" => $email,
					"firstName" => $firstName,
					"lastName" => $lastName,
					"password" => $password
				];

				if(!empty($id)) {
					$this->UserModel->update($obj, $id);
				} else {
					$this->UserModel->insert($obj);
				}
				header("Location: /users");
			} else {
				$data["errors"] = $errors;
				$this->layout->view('user/modify', $data);
			}
		} else {
			$data = [];

			if(!empty($userId)) {
				$user = $this->UserModel->get($userId);
				if(!empty($user)) {
					$data["user"] = $user;
				}
			} 

			$this->layout->view('user/modify', $data);
		}
	}

	public function login() {

		if(empty($this->session) || !$this->session->verified) {
			require_once './application/includes/duo_php/src/Web.php';

			$data = ["verified" => false];

			define('AKEY', "THISISMYSUPERSECRETCUSTOMERKEYDONOTSHARE");
			define('IKEY', "DI474CVFDVWWHNHPXO9P");
			define('SKEY', "uYHUWZfhXItEFQ8qGET3WbNmLdh1qSKvkCaJWGDG");

			if(!empty($_POST)) {

				if(!empty($_POST["email"]) && !empty($_POST["pass"])) {
					$email = $_POST["email"];
					$password = $_POST["pass"];

					$result = $this->UserModel->login($email, $password);

					if($result) {
						$sig_request = Duo\Web::signRequest(IKEY, SKEY, AKEY, $email);
						$user = ["userId" => $result["userId"], "email" => $email];
						$this->session->set_userdata($user);
						$data["host"] =  "api-3237f7a6.duosecurity.com";
			    		$data["sig_request"] = $sig_request;
					} else {
						$data["message"] = "Invalid Login Credentials";
						$data["invalid"] = true;
					}
				} else if (!empty($_POST['sig_response'])) {
				    $resp = Duo\Web::verifyResponse(IKEY, SKEY, AKEY, $_POST['sig_response']);
				    if($this->session->email == $resp) {
				    	$result = $this->UserModel->get($this->session->userId);
				    	if(!empty($result)) {
				    		$result["verified"] = true;
				    		$this->session->set_userdata($result);
				    		header("Location: /");
				    	}
				    }
				}
			}

			$this->layout->view('user/login', $data);
		} else {
			header("Location: /");
		}		
	}

	public function forgotpassword() {
		$data = [];

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$email = $_POST["email"];
			
			$where = ["email" => $email];

			$results = $this->UserModel->get_where($where);

			if(!empty($results)) {
				$user = $results[0];

				if(!empty($user)) {
					$guid = uniqid();
					$data = [
						"guid" => $guid,
						"allowPasswordReset" => 1,
						"modifyDate" => date("Y-m-d H:i:s")
					];
					$this->UserModel->update($data, $user["userId"]);

					$body = "<p>You're receiving this email because a password reset request was requested from Soo Bars with your email address.</p>";
					$body .= "<p>To reset your password, please click the link below:</p>";
					$body .= "<p><a href='http://aev.tekmtn.com/resetpassword/" . $user["userId"] . "/$guid'>Reset My Password</a></p>";

					$args = [
						"to" => $user["email"],
						"from" => "do-not-reply@tekmtn.com",
						"bcc" => "mike@tekmtn.com",
						"subject" => "AEV - Reset Your Password Request",
						"body" => $body
					];	

					$emailsent = $this->functions->sendphpmail($args);

					$data["emailsent"] = $emailsent;
					$data["message"] = "An email has been sent to your email address with a link to reset your password.";
				}					
			}
		}

		$this->layout->view('user/forgotpassword', $data);
	}

	public function logout() {
		$this->session->sess_destroy();
		header("Location: /login");
	}

	public function resetpassword($uid, $guid) {
		$data = ["reset" => false];

		$valid = $this->UserModel->verifyuser($uid, $guid);
		if($valid) {
			if($_SERVER["REQUEST_METHOD"] == "POST") {
				$password = $_POST["password"];

				$data = [
					"password" => $password,
					"allowPasswordReset" => 0,
					"modifyDate" => date("Y-m-d H:i:s")
				];

				$this->UserModel->update($data, $uid);

				$data["message"] = "Your password has been updated.";
				$data["reset"] = true;
			} else {
				$this->layout->view("user/resetpassword", $data);
			}
		}
	}
}