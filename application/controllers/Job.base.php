<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {controller_name}.php
defined('BASEPATH') OR exit('No direct script access allowed');

class JobBase extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("JobModel");
		$this->model = $this->JobModel;
	}

	public function index()
	{

	}

	public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$job = $post->job;
				
				$success = false;
				if(!empty($job->id)) {
					$this->JobModel->delete($job->id);
					$success = true;
				}

				$obj = [
					"success" => $success,
					"job" => $job
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$job = $post->job;
				
				$data = [
					"vehicleId" => $job->vehicleId,
					"job" => $job->job
				];

				$success = false;
				if(!empty($job->id)) {
					$this->JobModel->update($data, $job->id);
					$success = true;
				} else {
					$id = $this->JobModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function getall() {
		$results = $this->JobModel->get_all();
		header('Content-Type: application/json');
		echo json_encode($results, JSON_NUMERIC_CHECK);
	}


}
