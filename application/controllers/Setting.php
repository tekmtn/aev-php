<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'Setting.base.php';

class Setting extends SettingBase {
	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		
	}

	public function getall() {
		$results = $this->SettingModel->get_all();


		if(count($results) > 0) {
			header('Content-Type: application/json');
			echo json_encode($results[count($results)-1]);
		}

		
		
		//echo json_encode($results);
	}

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$settings = $post->json;
				
				$data = [
					"json" => $settings
				];

				$success = false;
				/*if(!empty($setting->id)) {
					$this->SettingModel->update($data, $setting->id);
					$success = true;
				} else {*/
					$id = $this->SettingModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				//}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj);
			}
		}
	}
}
