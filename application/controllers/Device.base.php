<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {controller_name}.php
defined('BASEPATH') OR exit('No direct script access allowed');

class DeviceBase extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("DeviceModel");
		$this->model = $this->DeviceModel;
	}

	public function index()
	{

	}

	/*public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$device = $post->device;
				
				$success = false;
				if(!empty($device->id)) {
					$this->DeviceModel->delete($device->id);
					$success = true;
				}

				$obj = [
					"success" => $success,
					"device" => $device
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}*/

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$device = $post->device;
				
				$data = [
					"macAddress" => $device->macAddress,
					"activated" => $device->activated,
					"associatedVIN" => $device->associatedVIN
				];

				$success = false;
				if(!empty($device->id)) {
					$this->DeviceModel->update($data, $device->id);
					$success = true;
				} else {
					$id = $this->DeviceModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function getall() {
		$results = $this->DeviceModel->get_all();
		header('Content-Type: application/json');
		echo json_encode($results, JSON_NUMERIC_CHECK);
	}


}
