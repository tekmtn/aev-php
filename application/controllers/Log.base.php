<?php
//do not make changes to this document, your changes will get overwritten!
//if you need to modify this class, modify the class that extends this class, most likely {controller_name}.php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogBase extends MY_Controller {
	function __construct() {
		parent::__construct();

		$this->load->model("LogModel");
		$this->model = $this->LogModel;
	}

	public function index()
	{

	}

	public function delete() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$log = $post->log;
				
				$success = false;
				if(!empty($log->id)) {
					$this->LogModel->delete($log->id);
					$success = true;
				}

				$obj = [
					"success" => $success,
					"log" => $log
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function save() {
		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$postdata = file_get_contents("php://input");
			if(!empty($postdata)) {
				$post = json_decode($postdata);
				$log = $post->log;
				
				$data = [

				];

				$success = false;
				if(!empty($log->id)) {
					$this->LogModel->update($data, $log->id);
					$success = true;
				} else {
					$id = $this->LogModel->insert($data);
					if(!empty($id)) {
						$success = true;
					}
				}

				$obj = [
					"success" => $success
				];

				header('Content-Type: application/json');
				echo json_encode($obj, JSON_NUMERIC_CHECK);
			}
		}
	}

	public function getall() {
		$results = $this->LogModel->get_all();
		header('Content-Type: application/json');
		echo json_encode($results, JSON_NUMERIC_CHECK);
	}


}
