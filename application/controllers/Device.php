<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'Device.base.php';

class Device extends DeviceBase {
	function __construct() {
		parent::__construct();

		$this->layout->setMasterLayout('layout.php');
		$this->layout->setSubLayout('/device/layout.php');
	}

	public function index()
	{
		$data = [];
		$results = $this->DeviceModel->get_all();
		$data["devices"] = $results;
		$this->layout->view('device/index', $data);
	}

	public function delete($deviceId) {
		$device = $this->DeviceModel->get($deviceId);

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			if(isset($_POST["delete"])) {
				$this->DeviceModel->delete($deviceId);

				header("Location: /devices");
			}
		} else {
			if(!empty($device)) {
				$data['device'] = $device;
				$this->layout->view('device/delete', $data);
			}
		}
	}

	public function modify($deviceId = null) {
		if($_SERVER["REQUEST_METHOD"] == "POST") {

			$errors = [];
			if(empty($_POST["year"])) {
				$errors[] = "Please enter device's year.";
			}
			if(empty($_POST["make"])) {
				$errors[] = "Please enter device's make.";
			}
			if(empty($_POST["model"])) {
				$errors[] = "Please enter device's model.";
			}
			
			$data["device"] = [];

			$data["device"]["year"] = $_POST["year"];
			$data["device"]["make"] = $_POST["make"];
			$data["device"]["model"] = $_POST["model"];
			$data["device"]["format"] = $_POST["format"];

			if(count($errors) == 0) {
				$id = $_POST["deviceId"];
				$year = $_POST["year"];
				$make = $_POST["make"];
				$model = $_POST["model"];
				$format = $_POST["format"];

				$obj = [
					"year" => $year,
					"make" => $make,
					"model" => $model,
					"format" => $format
				];

				if(!empty($id)) {
					$this->DeviceModel->update($obj, $id);
				} else {
					$this->DeviceModel->insert($obj);
				}
				header("Location: /devices");
			} else {
				$data["errors"] = $errors;
				$this->layout->view('device/modify', $data);
			}
		} else {
			$data = [];

			if(!empty($deviceId)) {
				$device = $this->DeviceModel->get($deviceId);
				if(!empty($device)) {
					$data["device"] = $device;
				}
			} 

			$this->layout->view('device/modify', $data);
		}
	}	
}
