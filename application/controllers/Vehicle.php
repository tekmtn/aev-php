<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'Vehicle.base.php';

class Vehicle extends VehicleBase {
	function __construct() {
		parent::__construct();



		if(empty($this->session->userdata) || $this->session->type != "Admin") {
			header("Location: /login");
		}

		$this->layout->setMasterLayout('layout.php');
		$this->layout->setSubLayout('/vehicle/layout.php');
	}

	public function index() {

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			$type = !empty($_POST["type"]) ? $_POST["type"] : null;
			$year = !empty($_POST["year"]) ? $_POST["year"] : null;
			$make = !empty($_POST["make"]) ? $_POST["make"] : null;
			$model = !empty($_POST["model"]) ? $_POST["model"] : null;
			$options = !empty($_POST["options"]) ? $_POST["options"] : null;
			$option_name = !empty($_POST["option_name"]) ? $_POST["option_name"] : null;

			$make_code = !empty($_POST["make_code"]) ? $_POST["make_code"] : null;
			$model_code = !empty($_POST["model_code"]) ? $_POST["model_code"] : null;
			$option_code = !empty($_POST["option_code"]) ? $_POST["option_code"] : null;

			$year_id = !empty($_POST["year_id"]) ? $_POST["year_id"] : null;
			$make_id = !empty($_POST["make_id"]) ? $_POST["make_id"] : null;
			$model_id = !empty($_POST["model_id"]) ? $_POST["model_id"] : null;
			$option_id = !empty($_POST["option_id"]) ? $_POST["option_id"] : null;

			if(!empty($option_id)) {
				// edit? delete?
				if($type == "delete") {
					$this->VehicleModel->delete_option($option_id);
				}

			} else if(!empty($model_id)) {
				// edit? delete? create?
				if($type == "create") {
					$data = [
						"model_id" => $model_id,
						"name" => $option_name,
						"options" => $options,
						"code" => $option_code
					];

					$option_id = $this->VehicleModel->insert_option($data);
				} else if($type == "delete") {
					$this->VehicleModel->delete_model($model_id);
				}

			} else if(!empty($make_id)) {
				// edit? delete? create?
				if($type == "create") {

					$model_id = $this->VehicleModel->find_model($make_id, $model);
					if(empty($model_id)) {
						$data = [
							"make_id" => $make_id,
							"model" => $model,
							"code" => $model_code
						];

						$model_id = $this->VehicleModel->insert_model($data);
					}
					
					if(!empty($model_id) && !empty($options)) {
						$data = [
							"model_id" => $model_id,
							"name" => $option_name,
							"options" => $options,
							"code" => $option_code
						];

						$option_id = $this->VehicleModel->insert_option($data);
					}
				} else if($type == "delete") {
					$this->VehicleModel->delete_make($make_id);
				}
			} else if(!empty($year_id) || !empty($year)) {

				$year = !empty($year_id) ? $year_id : $year;

				if($type == "create") {
					if(!empty($make)) {
						$data = [
							"year" => $year,
							"make" => $make,
							"code" => $make_code
						];

						$make_id = $this->VehicleModel->find_make($year, $make);
						if(empty($make_id)) {
							$make_id = $this->VehicleModel->insert_make($data);
						}

						if(!empty($model) && !empty($make_id)) {
							$model_id = $this->VehicleModel->find_model($make_id, $model);
							if(empty($model_id)) {
								$data = [
									"make_id" => $make_id,
									"model" => $model,
									"code" => $model_code
								];

								$model_id = $this->VehicleModel->insert_model($data);
							}

							if(!empty($model_id) && !empty($options)) {
								$data = [
									"model_id" => $model_id,
									"name" => $option_name,
									"options" => $options,
									"code" => $option_code
								];

								$option_id = $this->VehicleModel->insert_option($data);
							}
						}
					} else {
						$rows = $this->VehicleModel->insert_new_year($year);
					}
				} else if($type == "delete") {
					$this->VehicleModel->delete_year($year);
				}
				
			}
		}

		$data = [];
		$results = $this->VehicleModel->get_tree_view();
		$data["years"] = $results;

		$this->layout->view('vehicle/index', $data);
	}

	public function get_tree() {
		$data = $this->VehicleModel->get_tree_view();
		header('Content-Type: application/json');
		echo json_encode($data, JSON_NUMERIC_CHECK);
	}

	public function delete($vehicleId) {
		$vehicle = $this->VehicleModel->get($vehicleId);

		if($_SERVER["REQUEST_METHOD"] == "POST") {
			if(isset($_POST["delete"])) {
				$this->VehicleModel->delete($vehicleId);

				header("Location: /vehicles");
			}
		} else {
			if(!empty($vehicle)) {
				$data['vehicle'] = $vehicle;
				$this->layout->view('vehicle/delete', $data);
			}
		}
	}

	public function modify($vehicleId = null) {
		if($_SERVER["REQUEST_METHOD"] == "POST") {

			$errors = [];
			if(empty($_POST["year"])) {
				$errors[] = "Please enter vehicle's year.";
			}
			if(empty($_POST["make"])) {
				$errors[] = "Please enter vehicle's make.";
			}
			if(empty($_POST["model"])) {
				$errors[] = "Please enter vehicle's model.";
			}
			
			$data["vehicle"] = [];

			$data["vehicle"]["year"] = $_POST["year"];
			$data["vehicle"]["make"] = $_POST["make"];
			$data["vehicle"]["model"] = $_POST["model"];
			$data["vehicle"]["format"] = $_POST["format"];

			if(count($errors) == 0) {
				$id = $_POST["vehicleId"];
				$year = $_POST["year"];
				$make = $_POST["make"];
				$model = $_POST["model"];
				$format = $_POST["format"];

				$obj = [
					"year" => $year,
					"make" => $make,
					"model" => $model,
					"format" => $format
				];

				if(!empty($id)) {
					$this->VehicleModel->update($obj, $id);
				} else {
					$this->VehicleModel->insert($obj);
				}
				header("Location: /vehicles");
			} else {
				$data["errors"] = $errors;
				$this->layout->view('vehicle/modify', $data);
			}
		} else {
			$data = [];

			if(!empty($vehicleId)) {
				$vehicle = $this->VehicleModel->get($vehicleId);
				if(!empty($vehicle)) {
					$data["vehicle"] = $vehicle;
				}
			} 

			$this->layout->view('vehicle/modify', $data);
		}
	}	
}
