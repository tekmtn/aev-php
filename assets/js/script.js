$(document).ready(function() {

   var selected_node_id = null;

   $("#btn-cancel").click(function() {
      clearform();
      //console.log("selected_node_id", selected_node_id);
      if(selected_node_id) {
         $('#treeview-selectable').treeview('unselectNode', [ selected_node_id, { silent: true } ]);
      }
      
   });

   $("#btn-delete").click(function() {
      $(".selected-vehicle-header").text("Are you sure you want to delete this vehicle record?");
      $("#type").val('delete');

      $("#year-input").hide();
      $("#make-input").hide();
      $("#model-input").hide();
      $("#option-input").hide();
      $("#btn-delete").hide();
      $("#btn-save").text("Delete");
      $("#btn-save").removeClass("btn-primary").addClass("btn-danger").show();
   });

   function clearform() {
      $("#type").val('create');
      $("#year-input").show();
      $("#make-input").show();
      $("#model-input").show();
      $("#option-input").show();
      $("#year_id").val(null);
      $("#make_id").val(null);
      $("#model_id").val(null);
      $("#option_id").val(null);
      $("#btn-cancel").hide();
      $("#btn-delete").hide();
      $("#btn-save").removeClass("btn-danger").addClass("btn-primary").text("Save").show();
      $(".selected-vehicle-header").html(null);
      $(".create-vehicle-header").show();
   }

   function init() {
      $.get("/vehicle/get_tree", function( data ) {
         $('#treeview-selectable').treeview(
            {
               data: data,
               multiSelect: $('#chk-select-multi').is(':checked'),
               onNodeSelected: function(event, node) {
                  console.log("node", node);
                  selected_node_id = node.nodeId;

                  var is_edit = false;

                  if(node.year) {
                     is_edit = true;
                     $("#year-input").hide();
                     $("#year_id").val(node.year);
                  }
                  if(node.make_id) {
                     is_edit = true;
                     $("#make_id").val(node.make_id);
                     $("#make-input").hide();
                  }
                  if(node.model_id) {
                     is_edit = true;
                     $("#model_id").val(node.model_id);
                     $("#model-input").hide();
                  }
                  if(node.option_id) {
                     is_edit = true;
                     $("#option_id").val(node.option_id);
                     $("#option-input").hide();
                     $("#btn-save").hide();
                  }

                  if(is_edit) {
                     //$("#type").val('edit');
                     $("#btn-cancel").show();
                     $("#btn-delete").show();

                     var desc = "";
                     if(node.year) {
                        desc += node.year;
                     }
                     if(node.make) {
                        desc += " " + node.make;

                        if(node.make_code) {
                           desc += " (" + node.make_code + ")";
                        }
                     }
                     if(node.model) {
                        desc += " " + node.model;

                        if(node.model_code) {
                           desc += " (" + node.model_code + ")";
                        }
                     }
                     if(node.option_name) {
                        desc += " " + node.option_name;

                        if(node.option_code) {
                           desc += " (" + node.option_code + ")";
                        }
                     }

                     $(".create-vehicle-header").hide();
                     $(".selected-vehicle-header").html("<h3>" + desc + "</h3>").show();
                  } else {
                     $("#btn-cancel").hide();
                     $("#btn-delete").hide();

                     $(".create-vehicle-header").show();
                     $(".selected-vehicle-header").hide();
                  }

                  console.log("event", event, "node", node);
               },
               onNodeUnselected: function (event, node) {
                  clearform();
                  /*$(".edit-vehicle-form").hide();
                  $(".add-vehicle-form").show();

                  console.log("unselect event", event, "node", node);*/
               }
            }
         );
      });
   }

   init();
});